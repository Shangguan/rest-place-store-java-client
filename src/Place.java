import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * User: Shangguan
 * Date: 9/13/12
 */
public class Place {
    public Geometry geometry;
    public Properties properties;

    @Override
    public String toString() {
        StringBuilder formattedString = new StringBuilder();
        formattedString.append(geometry).append(" ").append(properties);
        return formattedString.toString();
    }
}
