import com.google.gson.Gson;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;

/**
 * User: Shangguan
 * Date: 9/13/12
 */

public class PlaceDemo {
    public static void main(String[] args) throws Exception {
        URL url = new URL("http://localhost:5000/places/near?latitude=30.267153&longitude=-97.7430608");
        URLConnection connection = url.openConnection();
        Reader data = new InputStreamReader(connection.getInputStream());
        Gson gson = new Gson();
        Places places = gson.fromJson(data, Places.class);

        StringBuilder formattedString = new StringBuilder();
        for (Place i : places.places) {
            formattedString.append(i).append("\n");
        }
        System.out.println(formattedString.subSequence(0, formattedString.length() - 1));
    }
}
