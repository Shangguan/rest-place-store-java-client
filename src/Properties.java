import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * User: Shangguan
 * Date: 9/13/12
 */
public class Properties {
    public String name;
    public int distance;
    public String address;
    public String city;
    public String state;
    public String postalCode;
    public String country;
    public String[] categories;

    @Override
    public String toString() {
        StringBuilder formattedString = new StringBuilder();
        formattedString.append(name)
                .append(" (").append(distance).append("m): ")
                .append(address).append(", ").append(city).append(", ").append(state)
                .append(" ").append(postalCode).append(", ").append(country).append(". ");
        formattedString.append("Categories: ");
        for (String category : categories) {
            formattedString.append(category).append(", ");
        }

        return formattedString.substring(0, formattedString.length() - 2);
    }
}
