/**
 * User: Shangguan
 * Date: 9/13/12
 */
public class Geometry {
    public String type;
    public double[] coordinates;

    @Override
    public String toString() {
        StringBuilder formattedString = new StringBuilder();
        formattedString.append("This is a ")
                .append(type).append(" at (")
                .append(coordinates[1]).append(", ")
                .append(coordinates[0]).append(")");
        return formattedString.toString();
    }
}
